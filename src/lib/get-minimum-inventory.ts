import { getItems } from "./items.js";
import { getQuestById } from "./quests.js";
import { ItemId } from "./types/item-id.js";
import { Quest } from "./types/quest.js";
import { QuestId } from "./types/quest-id.js";
import { QuestStatus } from "./types/quest-status.js";

/**
 * Returns the minimum count of each item given the supplied progress.
 * Note that this is a safe minimum: it may be that some quests in progress do not contribute any
 * item because it is impossible to know which item caused the progress.
 *
 * @param questStatuses Quests with their minimum status.
 * @param unlockedItems Set of ids of the unlocked items.
 * @return Item counts for the minimum inventory.
 */
export function getMinimumInventory(
  questStatuses: Map<QuestId, QuestStatus>,
  unlockedItems: Set<ItemId>,
): Map<ItemId, number> {
  const result: Map<ItemId, number> = new Map();

  for (const [questId, questStatus] of questStatuses) {
    const quest: Quest = getQuestById(questId);

    // Give the items for completed quests
    if (questStatus === QuestStatus.Completed) {
      for (const [itemId, required] of quest.require) {
        setMaxCount(result, itemId, required);
      }
    }

    // Give one item for in-progress quests with a single require
    if (questStatus === QuestStatus.InProgress && quest.require.size === 1) {
      const itemId: ItemId = quest.require.keys().next().value;
      setMaxCount(result, itemId, 1);
    }
  }

  // Clamp up or down with unlocks
  for (const item of getItems()) {
    if (unlockedItems.has(item.id)) {
      setMaxCount(result, item.id, item.unlock);
    } else {
      setMinCount(result, item.id, Math.max(item.unlock - 1, 0));
    }
  }

  return result;
}

/**
 * Updates the count of the item `itemId` in `inventory` by the minimum between the old count
 * and `value`. If `itemId` is missing, it is considered to be `0`.
 *
 * @param inventory Map of item counts
 * @param itemId Id of the item to update
 * @param value Value to compare with the old count
 */
function setMinCount(inventory: Map<ItemId, number>, itemId: ItemId, value: number) {
  const count: number | undefined = inventory.get(itemId);
  inventory.set(itemId, Math.min(count !== undefined ? count : 0, value));
}

/**
 * Updates the count of the item `itemId` in `inventory` by the maximum between the old count
 * and `value`. If `itemId` is missing, it is considered to be `0`.
 *
 * @param inventory Map of item counts
 * @param itemId Id of the item to update
 * @param value Value to compare with the old count
 */
function setMaxCount(inventory: Map<ItemId, number>, itemId: ItemId, value: number) {
  const count: number | undefined = inventory.get(itemId);
  inventory.set(itemId, Math.max(count !== undefined ? count : 0, value));
}
