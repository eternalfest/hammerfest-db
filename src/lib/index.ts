import { Incident } from "incident";

throw new Incident(
  "IndexRequire",
  "`hammerfest-db` does not export an index, use deep imports.",
);
