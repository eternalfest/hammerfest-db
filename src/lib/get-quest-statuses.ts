import { getQuests } from "./quests.js";
import { ItemId } from "./types/item-id.js";
import { QuestId } from "./types/quest-id.js";
import { QuestStatus } from "./types/quest-status.js";

/**
 * Returns the quest statuses corresponding to the supplied item counts.
 * Missing items in the inventory are assumed to have a count of `0`.
 *
 * @param inventory Map from item ids to item counts
 * @return Map from quest ids to quest statuses.
 */
export function getQuestStatuses(inventory: Map<ItemId, number>): Map<QuestId, QuestStatus> {
  const result: Map<QuestId, QuestStatus> = new Map();

  for (const quest of getQuests()) {
    // The inventory contains at least one item required by the quest
    let inProgress: boolean = false;
    // The inventory contains enough of each item required by the quest
    let completed: boolean = true;

    for (const [itemId, required] of quest.require) {
      let itemCount: number | undefined = inventory.get(itemId);
      if (itemCount === undefined) {
        itemCount = 0;
      }

      if (itemCount < required) {
        completed = false;
      }
      if (itemCount > 0) {
        inProgress = true;
      }
    }

    if (completed) {
      result.set(quest.id, QuestStatus.Completed);
    } else if (inProgress) {
      result.set(quest.id, QuestStatus.InProgress);
    } else {
      result.set(quest.id, QuestStatus.Inactive);
    }
  }

  return result;
}
