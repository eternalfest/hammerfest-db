import { Incident } from "incident";

import { ItemId } from "../types/item-id.js";

export type Name = "QuestNotFound";
export const name: Name = "QuestNotFound";

export interface Data {
  id: ItemId;
}

export type Cause = undefined;
export type QuestNotFoundError = Incident<Data, Name, Cause>;

export function format({id}: Data): string {
  return `Quest not found for the ID: ${id}`;
}

export function createQuestNotFoundError(id: ItemId): QuestNotFoundError {
  return Incident(name, {id}, format);
}
