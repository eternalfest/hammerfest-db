import { ScoreItem } from "./score-item.js";
import { SpecialItem } from "./special-item.js";

export type Item = SpecialItem | ScoreItem;
