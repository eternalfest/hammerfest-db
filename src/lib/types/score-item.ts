import { AbstractItem } from "./abstract-item.js";
import { DynamicScore } from "./dynamic-score.js";
import { ItemType } from "./item-type.js";

export interface ScoreItem extends AbstractItem {
  type: ItemType.Score;
  value: DynamicScore | number;
}
