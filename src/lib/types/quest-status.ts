export enum QuestStatus {
  Inactive,
  InProgress,
  Completed,
}
