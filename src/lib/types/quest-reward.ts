import { TaggedUnionType } from "kryo/tagged-union";

import {
  $BankQuestReward,
  $FamilyQuestReward,
  $LogQuestReward,
  $ModeQuestReward,
  $OptionQuestReward,
  $TokensQuestReward,
  BankQuestReward,
  FamilyQuestReward,
  LogQuestReward,
  ModeQuestReward,
  OptionQuestReward,
  TokensQuestReward,
} from "./quest-rewards.js";

export type QuestReward = BankQuestReward
  | FamilyQuestReward
  | LogQuestReward
  | ModeQuestReward
  | OptionQuestReward
  | TokensQuestReward;

export const $QuestReward: TaggedUnionType<QuestReward> = new TaggedUnionType<QuestReward>({
  variants: [
    $BankQuestReward,
    $FamilyQuestReward,
    $LogQuestReward,
    $ModeQuestReward,
    $OptionQuestReward,
    $TokensQuestReward,
  ],
  tag: "type",
});
