import { AbstractItem } from "./abstract-item.js";
import { ItemType } from "./item-type.js";

export interface SpecialItem extends AbstractItem {
  type: ItemType.Special;
}
