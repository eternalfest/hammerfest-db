import { ItemId } from "./item-id.js";
import { QuestId } from "./quest-id.js";
import { QuestReward } from "./quest-reward.js";
import { FamilyQuestReward } from "./quest-rewards.js";

export interface Quest {
  id: QuestId;
  require: Map<ItemId, number>;
  give: Set<QuestReward>;
  remove: Set<FamilyQuestReward>;
}
