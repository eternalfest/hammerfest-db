import chai from "chai";
import objectInspect from "object-inspect";

import { getQuestStatuses } from "../lib/get-quest-statuses.js";
import { ItemId } from "../lib/types/item-id.js";
import { QuestId } from "../lib/types/quest-id.js";
import { QuestStatus } from "../lib/types/quest-status.js";

describe("getQuestStatuses", function () {
  interface TestItem {
    name: string;
    inventory: Map<ItemId, number>;
    expected: Map<QuestId, QuestStatus>;
  }

  const testItems: TestItem[] = [
    {
      name: "No items",
      inventory: new Map(),
      expected: new Map(),
    },
    {
      name: "Master jeweller in progress and unlocked Moon Glade",
      inventory: new Map([
        ["1009", 0],
        ["1010", 5],
        ["1011", 10],
      ]),
      expected: new Map([["20", QuestStatus.InProgress]]),
    },
    {
      name: "Master jeweller in progress and unlocked Moon Glade",
      inventory: new Map([
        ["0", 43],
        ["1", 1],
        ["3", 1],
        ["4", 2],
        ["5", 1],
        ["7", 3],
        ["10", 4],
        ["13", 3],
        ["18", 1],
        ["21", 3],
        ["64", 1],
        ["82", 2],
        ["103", 1],
        ["104", 1],
        ["117", 1],
        ["1000", 70],
        ["1003", 3],
        ["1008", 4],
        ["1013", 2],
        ["1027", 1],
        ["1041", 1],
        ["1043", 1],
        ["1051", 1],
        ["1221", 3],
        ["1224", 1],
      ]),
      expected: new Map([
        ["7", QuestStatus.InProgress], // Some sugar!
        ["12", QuestStatus.InProgress], // Find the secret golden coins!
        ["14", QuestStatus.InProgress], // Armageddon
        ["18", QuestStatus.InProgress], // Difaïned treasure
        ["29", QuestStatus.InProgress], // Igor and Cortex
        ["35", QuestStatus.InProgress], // Game purchase enhanced
        ["67", QuestStatus.InProgress], // Sandy's adventure begins!
        ["2", QuestStatus.Completed], // First steps
        ["3", QuestStatus.Completed], // The adventure begins
        ["6", QuestStatus.Completed], // Delicacies
        ["10", QuestStatus.Completed], // Technological advance
        ["65", QuestStatus.Completed], // The adventure continues!
      ]),
    },
  ];

  for (const {name, inventory, expected} of testItems) {
    it(`Should return the expected inventory: ${name}`, function () {
      const actual: Map<QuestId, QuestStatus> = getQuestStatuses(inventory);
      chai.assert.strictEqual(actual.size, 76);
      for (const [questId, questStatus] of actual) {
        if (questStatus === QuestStatus.Inactive) {
          actual.delete(questId);
        }
      }
      try {
        chai.assert.deepEqual(actual, expected);
      } catch (err) {
        chai.assert.fail(
          actual,
          expected,
          `expected ${objectInspect(actual)} to deeply equal ${objectInspect(expected)}`,
        );
      }
    });
  }
});
