import chai from "chai";
import objectInspect from "object-inspect";

import { getMinimumInventory } from "../lib/get-minimum-inventory.js";
import { ItemId } from "../lib/types/item-id.js";
import { QuestId } from "../lib/types/quest-id.js";
import { QuestStatus } from "../lib/types/quest-status.js";

describe("getMinimumInventory", function () {
  interface TestItem {
    name: string;
    questStatuses: Map<QuestId, QuestStatus>;
    unlockedItems: Set<ItemId>;
    expected: Map<ItemId, number>;
  }

  const testItems: TestItem[] = [
    {
      name: "Trust items over quests",
      questStatuses: new Map([["12", QuestStatus.Completed]]),
      unlockedItems: new Set(),
      expected: new Map([["1051", 9]]),
    },
    {
      name: "Master jeweller in progress and unlocked Moon Glade",
      questStatuses: new Map([["20", QuestStatus.InProgress]]),
      unlockedItems: new Set(["1011"]),
      expected: new Map([["1011", 10]]),
    },
    {
      name: "Hammerfest identity b7c66599-c82e-4389-84ef-0aae19b57f29 (xmiax@fr)",
      questStatuses: new Map([
        ["7", QuestStatus.InProgress], // Some sugar!
        ["12", QuestStatus.InProgress], // Find the secret golden coins!
        ["14", QuestStatus.InProgress], // Armageddon
        ["18", QuestStatus.InProgress], // Difaïned treasure
        ["29", QuestStatus.InProgress], // Igor and Cortex
        ["35", QuestStatus.InProgress], // Game purchase enhanced
        ["67", QuestStatus.InProgress], // Sandy's adventure begins!
        ["2", QuestStatus.Completed], // First steps
        ["3", QuestStatus.Completed], // The adventure begins
        ["6", QuestStatus.Completed], // Delicacies
        ["10", QuestStatus.Completed], // Technological advance
        ["65", QuestStatus.Completed], // The adventure continues!
      ]),
      unlockedItems: new Set(["0", "103", "104", "1000"]),
      expected: new Map([
        ["0", 10], // Cristal Alphabet
        ["4", 1], // Magic Lamp
        ["7", 1], // Running Shoes
        ["10", 1], // Telephone
        ["13", 1], // Biker Helmet
        ["103", 1], // Wonderful Life!
        ["104", 1], // Heart of an Adventurer
        ["117", 1], // Gordon's Key
        ["1000", 10], // Hammerfest Crystal
        ["1003", 1], // Seed Candy
        ["1008", 1], // Difaïned Diamond
        ["1013", 1], // Muffin
        ["1027", 1], // Red Licorice
        ["1041", 1], // Lollipop
        ["1043", 1], // Plastic Sorbet
        ["1051", 1], // Secret Coin
      ]),
    },
  ];

  for (const {name, questStatuses, unlockedItems, expected} of testItems) {
    it(`Should return the expected inventory: ${name}`, function () {
      const actual: Map<ItemId, number> = getMinimumInventory(questStatuses, unlockedItems);
      chai.assert.strictEqual(actual.size, 353);
      for (const [itemId, count] of actual) {
        if (count === 0) {
          actual.delete(itemId);
        }
      }
      try {
        chai.assert.deepEqual(actual, expected);
      } catch (err) {
        chai.assert.fail(
          actual,
          expected,
          `expected ${objectInspect(actual)} to deeply equal ${objectInspect(expected)}`,
        );
      }
    });
  }
});
